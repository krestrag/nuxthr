import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default ()=> new Vuex.Store({
  state: {
    news: [
      {
        id: 192,
        publicationDate: "12 авг 2020",
        header:"Круто мыслить позитивно, быть здоровым, жить активно!",
        body: "Сегодня в свой обеденный перерыв сотрудники управления государственной службы и кадровой работы администрации Липецкой области...",
        imgSrc: "news1.png"

      },
      {
        id: 202,
        publicationDate: "6 июня 2022",
        header:"Более 40 жителей Липецкой области получили заслуженные награды",
        body: "Церемония вручения государственных, ведомственных и региональных наград жителям Липецкой области состоялась 3 июня в администрации региона.",
        imgSrc: "news3.jpeg"

      },
      {
        id: 193,
        publicationDate: "28 февр 2022",
        header:"Игорь Артамонов вручил награды жителям Липецкой области",
        body: "Торжественная церемония вручения государственных, ведомственных и региональных наград, приуроченная ко Дню защитника Отечества состоялась в администра...",
        imgSrc: "news2.jpg"
      },
      {
        id: 92,
        publicationDate: "12 авг 2020",
        header:"Предоставление муниципальных услуг в центрах “Мои документы” обретает единый формат",
        body: "",
        imgSrc: "news_template.png"
      },
      {
        id: 93,
        publicationDate: "12 авг 2020",
        header:"Состоялось заседание hr-клуба по вопросам повышения эффективности качества работы hr-служб в подведомственных учреждениях области",
        body: "",
        imgSrc: "news_template.png"
      },
      {
        id: 94,
        publicationDate: "12 авг 2020",
        header:"Липецкая область участвует в ярмарке вакансий в онлайн-формате",
        body: "",
        imgSrc: "news_template.png"
      },
      {
        id: 95,
        publicationDate: "12 авг 2020",
        header:"В рамках hr-форума стартует новый проект",
        body: "",
        imgSrc: "news_template.png"
      },
      {
        id: 96,
        publicationDate: "12 авг 2020",
        header:"В рамках hr-форума стартует новый проект",
        body: "",
        imgSrc: "news_template.png"
      },
      {
        id: 97,
        publicationDate: "12 авг 2020",
        header:"В рамках hr-форума стартует новый проект",
        body: "",
        imgSrc: "news_template.png"
      },
    ],
    managementInstitutions:[
      {
        name: "Отдел кадровой работы",
        icon: "books",
      },
      {
        name: "Отдел государственной службы",
        icon: "act",
      },
      {
        name: "Отдел кадровой экспертизы",
        icon: "folder",
      },
      {
        name: "Отдел наградной работы",
        icon: "prize",
      },
      {
        name: "Отдел развития кадрового потенциала",
        icon: "growth",
      },
    ]
  },
  getters: {
    getNews: state => state.news,
    getManagementInstitutions: state => state.managementInstitutions,
  },
  modules: {
  }
})
